<?php
/**
 * Content Page
 *
 * Loop content in page template (page.php)
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */
?>

<article>

	<header>
		<h1><?php the_title(); ?></h1>
	</header>

	<?php the_content(); ?>

</article>