<?php
/**
 * Content Aside
 *
 * Displays 'aside' custom post format
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */
?>

<article>

	<header>
		<hgroup>
			<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'qproject' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			<span class="right radius success label"><?php echo get_post_format(); ?></span>
			<h6>Written by <?php the_author_link(); ?></h6>
		</hgroup>
	</header>

	<?php if ( has_post_thumbnail()) : ?>
	<a href="<?php the_permalink(); ?>" class="th" title="<?php the_title_attribute(); ?>" ><?php the_post_thumbnail(); ?></a>
	<?php endif; ?>

	<?php the_excerpt(); ?>

</article>

<hr />