<?php

/**
 * Functions
 *
 * Core functionality and initial theme setup
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */


/**
 * Initiate qproject, for WordPress
 */

function qproject_setup() {

	// Language Translations
	load_theme_textdomain( 'qproject', get_template_directory() . '/languages' );

	// Custom Editor Style Support
	add_editor_style();

	// Support for Featured Images
	add_theme_support( 'post-thumbnails' ); 

	// Automatic Feed Links & Post Formats
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );

}
add_action( 'after_setup_theme', 'qproject_setup' );

/**
 * Enqueue Scripts and Styles for Front-End
 */

function qproject_assets() {

	if (!is_admin()) {

		// Load JavaScripts
		//wp_enqueue_script( 'qproject', get_template_directory_uri() . '/javascripts/qproject.min.js', array(), '1.0', true );
		//wp_enqueue_script( 'app', get_template_directory_uri().'/javascripts/app.js', array('qproject'), '1.0', true );
		
		// Load CSS
//		wp_enqueue_style( 'qproject', get_template_directory_uri().'/media.css' );
		wp_enqueue_style( 'app', get_stylesheet_uri() );

		// Load Google Fonts API
		wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' );
	
	}

}
add_action( 'wp_enqueue_scripts', 'qproject_assets' );


/* wp_register_script('js_custom', get_template_directory_uri() . '/js/inq_custom.js', array('jquery'));
  wp_enqueue_script( 'js_custom' );	
	*/
/**
 * Register Navigation Menus
 */

// Register wp_nav_menus
function qproject_menus() {

	register_nav_menus(
		array(
			'header-menu' => __( 'Header Menu', 'qproject' )
		)
	);
	
}
add_action( 'init', 'qproject_menus' );

// Create a graceful fallback to wp_page_menu
function qproject_page_menu() {

	$args = array(
	'sort_column' => 'menu_order, post_title',
	'menu_class'  => 'twelve columns',
	'include'     => '',
	'exclude'     => '',
	'echo'        => true,
	'show_home'   => false,
	'link_before' => '',
	'link_after'  => ''
	);

	wp_page_menu($args);

}

/**
 * Navigation Menu Adjustments
 */


// Add class to navigation sub-menu
class qproject_navigation extends Walker_Nav_Menu {

function start_lvl(&$output, $depth=0, $args=Array()) {
	$indent = str_repeat("\t", $depth);
	$output .= "\n$indent<ul class=\"flyout\">\n";
}

function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
	$id_field = $this->db_fields['id'];
	if ( !empty( $children_elements[ $element->$id_field ] ) ) {
		$element->classes[] = 'has-flyout';
	}
		Walker_Nav_Menu::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}
}

// Add a class to the wp_page_menu fallback
function qproject_page_menu_class($ulclass) {
	return preg_replace('/<ul>/', '<ul class="nav-bar">', $ulclass, 1);
}

add_filter('wp_page_menu','qproject_page_menu_class');

/**
 * Create pagination
 */

function qproject_pagination() {

global $wp_query;

$big = 999999999;

$links = paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'prev_next' => true,
	'prev_text' => '&laquo;',
	'next_text' => '&raquo;',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages,
	'type' => 'list'
)
);

$pagination = str_replace('page-numbers','pagination',$links);

echo $pagination;

}

/**
 * Register Sidebars
 */

function qproject_widgets() {

	// Home page widget
	register_sidebar( array(
			'id' => 'qproject_homepage-widget',
			'name' => __( 'Home Clients Slider', 'qproject' ),
			'description' => __( 'This widget is located in the home page.', 'qproject' ),
			'before_widget' => '<li>',
			'after_widget' => '</li>',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
		) );

	// Sidebar Right
	register_sidebar( array(
			'id' => 'qproject_sidebar_right',
			'name' => __( 'Sidebar Page', 'qproject' ),
			'description' => __( 'This sidebar is located on the right-hand side of each page.', 'qproject' ),
			'before_widget' => '<div class="sidebar-widget">',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );

	// Sidebar Support
	register_sidebar( array(
			'id' => 'qproject_sidebar_support',
			'name' => __( 'Sidebar Support', 'qproject' ),
			'description' => __( 'This sidebar is located on the right-hand side of the support page.', 'qproject' ),
			'before_widget' => '<div class="sidebar-widget">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
		) );

	// Sidebar Footer Column One
	register_sidebar( array(
			'id' => 'qproject_sidebar_footer_one',
			'name' => __( 'Sidebar Footer One', 'qproject' ),
			'description' => __( 'This sidebar is located in column one of your theme footer.', 'qproject' ),
			'before_widget' => '<div class="footer-widget">',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="col-md-2">',
			'after_title' => '</h2>',
		) );

	// Sidebar Footer Column Two
	register_sidebar( array(
			'id' => 'qproject_sidebar_footer_two',
			'name' => __( 'Sidebar Footer Two', 'qproject' ),
			'description' => __( 'This sidebar is located in column two of your theme footer.', 'qproject' ),
			'before_widget' => '<div class="footer-widget">',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
		
	// Sidebar Blog one
	register_sidebar( array(
			'id' => 'qproject_sidebar_blog',
			'name' => __( 'Sidebar Blog One', 'qproject' ),
			'description' => __( 'First Blog sidebar (left)', 'qproject' ),
			'before_widget' => '<div class="sidebar-widget">',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );

	// Sidebar Blog two
	register_sidebar( array(
			'id' => 'qproject_sidebar_blog_two',
			'name' => __( 'Sidebar Blog Two', 'qproject' ),
			'description' => __( 'Second Blog sidebar (right)', 'qproject' ),
			'before_widget' => '<div class="sidebar-widget">',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );


	}

add_action( 'widgets_init', 'qproject_widgets' );

/**
 * HTML5 IE Shim
 */

function qproject_shim () {
    echo '<!--[if lt IE 9]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
    echo '<![endif]-->';
}

add_action('wp_head', 'qproject_shim');

/**
 * Custom Avatar Classes
 */

function qproject_avatar_css($class) {
	$class = str_replace("class='avatar", "class='author_gravatar left ", $class) ;
	return $class;
}

add_filter('get_avatar','qproject_avatar_css');

/**
 * Custom Post Excerpt
 */

function new_excerpt_more($more) {
    global $post;
	return '... <br><br><a class="small button secondary" href="'. get_permalink($post->ID) . '">Continue reading &raquo;</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

/**
 * Retrieve Shortcodes
 */

require( get_template_directory() . '/inc/shortcodes.php' );

/** Support functions for DE hierarchy  **/

if (!function_exists('get_big_daddy')) {
	function get_big_daddy($post, $ignore_ref = false)
	{
		$ancestors = get_post_ancestors($post);

		if (!$ancestors)
			return false;

		array_unshift($ancestors, $post->ID);
		if (sizeof($ancestors) > 0) {
			$i = array_pop($ancestors);
			if ($ignore_ref)
				return array_pop($ancestors);
			return $i;
		} else
			return $post->ID;
	}
}

if(!function_exists('get_top_level_parent_title')){
	function get_top_level_parent_title() {
	global $post;
		if ( empty($post->post_parent) )
			{ the_title(); }
		else {
			$parents = get_post_ancestors( $post->ID );
			$the_parent = apply_filters( "the_title", get_the_title( end ( $parents ) ) );
			$the_url = apply_filters( "the_permalink", get_permalink( end ( $parents ) ) );
    		echo '<a href="'.$the_url.'" title="'.$the_parent.'">'.$the_parent.'</a>';
		}
	}
}
/**
 * Sidebar Footer - Clients Logo
 */
 
function inqbation_clients_logos() {

  wp_register_style('css_flexslider', get_template_directory_uri() . '/js/flexslider/flexslider.css');
  wp_enqueue_style( 'css_flexslider' );

  wp_register_script('js_flexslider', get_template_directory_uri() . '/js/flexslider/jquery.flexslider-min.js', array('jquery'), '2.1', true);
  wp_enqueue_script( 'js_flexslider' );

  wp_register_script('js_custom', get_template_directory_uri() . '/js/inq_custom.js', array('jquery'), '1.0', true);
  wp_enqueue_script( 'js_custom' );

}
add_action( 'widgets_init', 'inqbation_clients_logos' );

if ( ! function_exists( 'inqbation_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 * Create your own twentyeleven_posted_on to override in a child theme
 *
 */
function inqbation_posted_on() {
	printf( __( '<span class="sep">Posted</span><span class="by-author"> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span></span>', 'qproject' ),
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		sprintf( esc_attr__( 'View all posts by %s', 'qproject' ), get_the_author() ),
		esc_html( get_the_author() )
	);
}
endif;

if ( ! function_exists( 'inqbation_posted_in' ) ) :
/**
 * Prints HTML with meta information for the current post (category, tags and permalink).
 *
 * @since Twenty Ten 1.0
 */
function inqbation_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;

function the_excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;
	global $post;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '...<p><a title="Read more" href="'. get_permalink($post->ID) . '">Read more &raquo;</a></p>';
	} else {
		echo $excerpt;
	}
}

//Functions to set/get the number of post views. Based on: http://stackoverflow.com/questions/9472602/wordpress-popular-posts-without-using-plugins
function getPostViews($postID){
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
		return "0 View";
	}
	return $count.' Views';
}

//This is used at the end of the single.php file, so it adds 1 each time a single post is viewed.
function setPostViews($postID) {
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	} else {
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}


function getPopularPosts($count) {
	$args = array( 'numberposts' => $count, 'meta_key' => 'post_views_count', 'orderby' => 'post_views_count', 'order' => 'DESC');
	$posts = get_posts( $args );
	$popular = '<ul class="cdc-popular">';

	foreach( $posts as $post ) {
		setup_postdata($post);
		$popular .= '<li><a href="' . get_permalink($post->ID) .'">' . get_the_title($post->ID) . /*'(' . getPostViews($post->ID) . ')' .*/ '</a></li>';
		//getPostViews($post->ID);
	}

	$popular .= '</ul>';
	return $popular;

}

//Function to get the tag name by id
function get_tag_name($id) {
  $id = (int) $id;
  global $wpdb;
  $name = $wpdb->get_var("SELECT name FROM $wpdb->terms WHERE term_id='$id'");
	if(!ctype_upper($name)):
		$name = ucwords(strtolower($name));
	endif;
  return $name;
}



function redirect_to_spanish() {
    // Redirects to the spanish website the first time a visitor (from Latam and Spain) goes to the homepage
    if(!$_COOKIE["redirected"]){
        setcookie("redirected", 1, time()+3600*24*7, "/"); //1 week

        if (( is_home() || is_front_page() ) && ($_GET['lang'] != "en") ) {

            //http://ipinfodb.com
            include('ipinfodb/ip2locationlite.class.php');

            $ip = $_SERVER["REMOTE_ADDR"];

            //Load the class
            $ipLite = new ip2location_lite;
            $ipLite->setKey('8589371609138e966020eae49266ba7a39b137d7b77021c17eb77f155b2246e6');
             
            //Get errors and locations
            $locations = $ipLite->getCountry($ip);
            $errors = $ipLite->getError();
            //setcookie("locations", base64_encode(serialize($locations)), time()+3600*24*7, "/"); //1 week
            //setcookie("errors", base64_encode(serialize($errors)), time()+3600*24*7, "/"); //1 week

            if ((empty($errors) || !is_array($errors)) && $locations['statusCode'] == 'OK') {
                $country = $locations['countryCode'];
                // $country = "CA";
                //setcookie("geo", $country, time()+3600*24*7, "/"); //1 week

                # Argentina, Bolivia, Chile, Colombia, Costa Rica, Cuba, Dominican Republic, Ecuador, El Salvador, Guatemala, Honduras, Mexico, Nicaragua, Panama, Paraguay, Peru, Puerto Rico, Uruguay, Venezuela, Spain
                $hispanic_countries = array("AR", "BO", "CL", "CO", "CR", "CU", "DO", "EC", "SV", "GT", "HN", "MX", "NI", "PA", "PY", "PE", "PR", "UY", "VE", "ES");

                if (in_array($country, $hispanic_countries)) {
                    //Header( "HTTP/1.1 301 Moved Permanently" );
                    Header( "Location: https://www.inqbation.com/es/" );
                }
            }
        }
    }
}

//Function to get the latest authors
function get_latest_authors( $number_of_authors = 5 ) {
	$args = array(
		'orderby' => 'modified',
		'post_type' => 'post',
		'post_status' => 'publish',
		'numberposts' => '-1'
	);
		
	$count = 1;
	$recent_posts = wp_get_recent_posts( $args );
	$latest_authors_array = array();
		
	foreach( $recent_posts as $the_post ) {
		if ( $count == $number_of_authors ) break;
		if ( ! in_array( $the_post['post_author'], $latest_authors_array ) ) {
			$latest_authors_array[] =$the_post['post_author'];
			$count++;
		}
	}

	$latest_authors = '<ul class="cdc-latest-authors">';
	foreach( $latest_authors_array as $author_id ) {
		$latest_authors .= '<li><a href="'.get_author_posts_url( $author_id ).'">'.get_the_author_meta( 'display_name', $author_id ).'</a></li>';
	}
	$latest_authors .= '</ul>';

	return $latest_authors;
}
?>