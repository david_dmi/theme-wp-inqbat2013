<?php

/*
 * Template Name: Contact Template
 */

get_header(); ?>

    <!-- Main Content -->
    <div id="content" class="grid_24" role="content">

    	<div class="content-inside">

		<div id="breadcrumbs" class="grid_24 alpha omega">
		    <?php
		        if(function_exists('bcn_display')){
		            bcn_display();
		        }
		    ?>
		</div>

		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'contact' ); ?>
			<?php endwhile; ?>
			
		<?php endif; ?>
		
	    </div><!-- End of content-inside -->
	</div><!-- End of content --> 

<?php get_footer(); ?>