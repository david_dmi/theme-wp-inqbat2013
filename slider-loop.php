			<?php 
			$my_url = urlencode(get_permalink($post->ID));
			$my_title = urlencode(get_the_title($post->ID));
			$my_text = urlencode( str_replace( "<a class=\"read-more\" href=\"".get_permalink($post->ID)."\">Read more <span class=\"meta-nav\">&raquo;</span></a>", "", get_the_excerpt($post->ID) ));

			if ( has_post_thumbnail($post->ID)):
				$my_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
				$my_img = $my_img[0];
			else :
				$my_img = get_bloginfo('template_directory');
				$my_img = $my_img . "/images/inQbation-thumbnail.png";
			endif;
		?>
		
		<div class="slide">
			<div class="slider-image prefix_1 grid_10">
				<a href="<?php the_permalink() ?>" title="Permalink to <?php the_title(); ?>" >
					<img src="<?php echo get_bloginfo('template_directory');?>/scripts/timthumb.php?src=<?php echo $my_img; ?>&amp;w=467&amp;h=268&amp;a=tl&amp;zc=1&amp;q=100" alt="<?php the_title_attribute(); ?>" />
				</a>
			</div>
			
			<div class="slider-content prefix_1 suffix_1 grid_11 alpha omega">

				<h2 class="entry-title"><a href="<?php the_permalink() ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				
				<div class="entry-meta">
					<?php inqbation_posted_on(); ?>
				</div><!-- .entry-meta -->
				
				<?php
					$textlong= strip_tags(get_the_content());
					//$textlong = get_the_excerpt();
					$permalink = get_permalink($post->ID);
					$textshort = limit_words($textlong, 30,'...' );
				?>	
						
				<div class="entry-summary">
					<div class="entry-content">
	          <p><?php echo $textshort; ?></p>
					</div>
					<div class="read-more"><a href="<?php echo $permalink; ?>">Read more &raquo;</a></div>
				</div><!-- .entry-summary -->
	      
				<div class="entry-utility inqbation-class">
					<span class="comments-link icon">
						<?php comments_popup_link( __( 'Leave a comment' ), __( '1 Comment' ), __( '% Comments' ) ); ?>
					</span>
				</div>
				
						<div class="follow-buttons-post">
			                <ul>
			                    <li><span class="title-follow">Share:</span></li>
			                    <?php
			                    	$my_url = urlencode(get_permalink($post->ID));
			                    	$my_title = urlencode(get_the_title($post->ID));
			                    	$my_text = urlencode( str_replace( "<a href=\"".get_permalink($post->ID)."\">Read more <span class=\"meta-nav\">&raquo;</span></a>", "", get_the_excerpt($post->ID) ));

			                    	if ( has_post_thumbnail($post->ID)){
			                    		$my_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID));
			                    		$my_img = $my_img[0];
			                    	} else {
			                    		$my_img = get_bloginfo('template_directory');
			                    		$my_img = $my_img . "/images/inQbation-thumbnail.jpg";
			                    	}
			                    	$my_img = urlencode($my_img);
			                    ?>

			                    <li><a href="https://twitter.com/share?text=<?php echo $my_title; ?>&amp;url=<?php echo $my_url; ?>&amp;via=inQbation&amp;lang=en" title="Share on Twitter" target="_blank" class="twitter-icon twitter popup icon">Twitter</a></li>

			                    <li><a href="http://www.facebook.com/sharer.php?s= 100&amp;p[title]=<?php echo $my_title; ?>&amp;p[url]=<?php echo $my_url; ?>&amp;p[images][0]=<?php echo $my_img; ?>&amp;p[summary]=<?php echo $my_text; ?>" target="_blank" title="Sahre on Facebook" class="facebook-icon icon">Facebook</a></li>

			                    <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $my_url; ?>&amp;title=<?php echo $my_title; ?>&amp;summary=<?php echo $my_text; ?>" target="_blank" title="Share on LinkedIn" class="linkedin-icon icon">LinkedIn</a></li>

			                    <li><a href="http://pinterest.com/pin/create/button/?url=<?php echo $my_url; ?>&amp;media=<?php echo $my_img; ?>&amp;description=<?php echo $my_title; ?>" target="_blank" title="Share on Pinterest" class="pinterest-icon icon">Pinterest</a></li>
			                    
			                    <li><a href="https://plus.google.com/share?url=<?php echo $my_url; ?>&amp;hl=en" target="_blank" title="Share on Google+" class="google-plus-icon icon">Google+</a></li>
			                    
			                </ul>
			            </div> <!-- .follow-buttons-post -->
    			</div>		
		
		</div>