<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */

    function homepage_big_image()
    {
	    if (have_posts()) {
		    $page = the_post();
		    if (has_post_thumbnail()) {
			    $css = '.page-heading {background-image: url(' . get_the_post_thumbnail_url($page, 'full') . ')!important;}';
			    wp_add_inline_style('app', $css);
		    }
	    }
    }
    add_action('wp_enqueue_scripts', 'homepage_big_image');

get_header();
?>

	<div class="full-width page-heading bottom-color-divider">
		<div class="banner">
			<h1 class="home-banner-title"><?php the_title(); ?>
				<span class="home-banner-tagline"><?php the_content(); ?></span>
			</h1>
		</div>

		<!-- clients -->
		<div id="clients-logos">
			<div class="container">
                <h3 class="title-clients">Our <span>Clients and Partners</span></h3>

                <div class="row">
				<ul class="clients-list">
					<li><img width="300" alt="NASA" src="<?php bloginfo('template_directory'); ?>/images/clients/inqbation-client-logos-nasa.png" class="img-responsive center-block"></li>
					<li><img width="300" alt="Agileana" src="<?php bloginfo('template_directory'); ?>/images/clients/inqbation-clients-logo-agileana.png" class="img-responsive center-block"></li>
					<li><img width="300" alt="Georgetown University" src="<?php bloginfo('template_directory'); ?>/images/clients/inqbation-clients-logo-georgetown-university.png" class="img-responsive center-block"></li>
					<li><img width="300" alt="Balystic" src="<?php bloginfo('template_directory'); ?>/images/clients/inqbation-clients-logo-balystic.png" class="img-responsive center-block"></li>
					<li><img width="300" alt="US Department of State" src="<?php bloginfo('template_directory'); ?>/images/clients/inqbation-client-logos-us-state-department.png" class="img-responsive center-block"></li>
				</ul>
                </div>
			</div>
		</div>
		<!-- End Clients -->
	</div>
	<!-- End home block -->

<?php get_footer(); ?>