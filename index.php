<?php
get_header();
?>
<div id="wrapper" class="container-fluid">
	<div class="row">

	<?php /*if (is_single()): ?>
		<?php get_template_part('single'); ?>

	<?php elseif (is_search()): ?>
		<?php get_template_part('category'); ?>

	<?php else: */?>

	<div id="content">
	<div class="col-md-8 col-md-push-2">

		<div id="breadcrumbs">
			<?php
			if (function_exists('bcn_display')):
				bcn_display();
			endif;
			?>
		</div>

		<h1>
			<?php
			$idBlogPage = get_option('page_for_posts');
			echo get_the_title($idBlogPage);
			?>
		</h1>

		<?php if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>

				<article <?php post_class('row'); ?>>
					<figure class="col-sm-4">
					<?php
						if (has_post_thumbnail($post->ID)):
							$my_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
							$my_img = $my_img[0];
						else :
							$my_img = get_bloginfo('template_directory');
							$my_img = $my_img . "/images/inQbation-thumbnail.png";
						endif;
						?>

						<a href="<?php the_permalink(); ?>">
							<img src="<?php echo $my_img ?>" alt="<?php the_title_attribute(); ?>" width="376" class="img-responsive">
						</a>
					</figure>

					<div class="col-sm-8">
						<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<small class="m-t-10" id="link-block">
							<i class="fa fa-user"></i> <?php the_author_posts_link(); ?>  &nbsp;&nbsp;&nbsp;
							<i class="fa fa-calendar"></i> <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date('F, Y'); ?></time> &nbsp;&nbsp;&nbsp;
							Filed under: <?php the_category(', '); ?>
						</small>
						<p><?php the_excerpt(); ?></p>
						<?php
						echo get_the_tag_list('<span>Tagged: ',', ','</span>');
						?>
					</div>
				</article>

				<hr>
			<?php endwhile; ?>
		<?php endif; ?>

	</div>
	</div>

<?php //endif; ?>

<?php get_template_part('blog-sidebar-one'); ?>

<?php get_template_part('blog-sidebar-two'); ?>

<?php get_footer(); ?>