<?php
/**
 * Footer
 *
 * Displays content shown in the footer section
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */
?>

<?php if (!is_front_page()): ?>
        </div>
    </div>
<?php endif; ?>
<!-- End Page -->




<!-- Footer -->
    <footer id="footer">


        <!-- map to google maps -->
        <div class="footer-map">
             <a href="http://maps.google.com/?saddr=Current+Location&amp;daddr=38.9595181,-77.3704265" target="_blank"> </a>
        </div>



        <div class="footer-top">
            <div id="footer-content" class="container">
                <div class="row">
<!--                    <div class="col-md-12">-->
                        <?php if (is_active_sidebar('qproject_sidebar_footer_one')) : ?><?php dynamic_sidebar('qproject_sidebar_footer_one'); ?>
                        <?php endif; ?>
<!--                    </div>-->
                </div>
            </div>
        </div>

        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div id="footer-menu" class="col-md-12">
                        <?php if (is_active_sidebar('qproject_sidebar_footer_two')) : ?>
                            <?php dynamic_sidebar('qproject_sidebar_footer_two'); ?>
                        <?php endif; ?>
                    </div>
<!--                <div id="copyright" class="grid_24">-->
<!--                    <a href="https://www.inqbation.com" target="_blank" title="Washington DC Web Designer">DC Web Designer</a> : <a href="https://www.inqbation.com" target="_blank" title="inQbation">inQbation</a>-->
<!--                </div>-->
                </div>
            </div>
        </div>

    </footer>
    <!-- End Footer -->

    <?php wp_footer(); ?>

    <!-- Facebook Like -->
    <div id="fb-root"></div>
</body>
</html>