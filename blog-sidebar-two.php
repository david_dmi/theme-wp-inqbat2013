<div id="sidebar-blog-right" class="sidebar-blog col-md-2 hidden-sm hidden-xs">
	<div class="inner">
		<div class="search-posts">
			<?php if (is_active_sidebar('qproject_sidebar_blog_two')) : ?>
				<?php dynamic_sidebar('qproject_sidebar_blog_two'); ?>
			<?php endif; ?>
		</div>

	</div>
</div>
