<?php
function limit_words($str, $num, $append_str = '')
{
	$words = preg_split('/[\s]+/', $str, -1, PREG_SPLIT_OFFSET_CAPTURE);
	if (isset($words[$num][1])):
		$str = substr($str, 0, $words[$num][1]) . $append_str;
	endif;
	unset($words, $num);
	return trim($str);
}

?>

<script>
	jQuery(function () {
		jQuery('#slides').slides({
			preload: false,
			preloadImage: '<?php echo get_bloginfo('template_directory')?>/images/img/loading.gif',
			play: 10000,
			pause: 2500,
			hoverPause: true,
			effect: 'fade',
			fadeSpeed: 500,
			crossfade: true,
			animationStart: function (current) {
				jQuery('.caption').animate({
					bottom: -35
				}, 100);
				jQuery('.slider-content').fadeIn("slow");

				if (window.console && console.log) {
					// example return of current slide number
					//console.log('animationStart on slide: ', current);
				}
				;
			},
			animationComplete: function (current) {
				jQuery('.caption').animate({
					bottom: 0
				}, 200);
				if (window.console && console.log) {
					// example return of current slide number
					//console.log('animationComplete on slide: ', current);
				}
				;
			},
			slidesLoaded: function () {
				jQuery('.caption').animate({
					bottom: 0
				}, 200);
			}
		});
	});
</script>

<div id="slides" class="grid_24 alpha omega">
    <div class="slides_container">

		<?php
		global $wp_query;

		$cat_id = get_cat_ID('featured');
		query_posts(array
		('post_type' => 'post',
			'orderby' => 'date',
			'order' => 'DESC',
			'showposts' => 5
		));

		while (have_posts()) : the_post();
			include 'slider-loop.php';
		endwhile; ?>

    </div>

</div>

<!-- Note: This slider was design by Orman Clark at <a href="http://www.premiumpixels.com/" target="_blank">Premium Pixels</a>. You can donwload the source PSD at http://www.premiumpixels.com/clean-simple-image-slider-psd/. � 2010 - Nathan Searles. All rights reserved. Slides is licensed under the Apache license -->	