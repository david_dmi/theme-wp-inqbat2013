//For update height on the boxes
var inQHelpers = ( function(my, $) {
		// add capabilities...
		
		var EMPTY_STRING = '';
		
		function equalHeight(group) {
			var tallest = 0;
			if(typeof group != 'undefined' && group.length != 0){
				group.each(function() {
					var thisHeight = $(this).height();
					if (thisHeight > tallest) {
						tallest = thisHeight;
					}
					console.log("group:" + group + tallest);
					
				});
				group.height(tallest);
			}
		}
		
		my.equalHeight = equalHeight;
		
		return my;
	}(inQHelpers || {}, jQuery)); 