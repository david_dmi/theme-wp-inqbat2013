
jQuery(document).ready(function() {
	// Handler for .ready() called
	jQuery(".cit-building a").click(function() {
		if (jQuery('.full-width') != null) {
			jQuery(".full-width").css('opacity', '0.0');
		}
		if (jQuery('#content') != null) {
			jQuery("#content").css("visibility", "hidden");
			jQuery("#content-footer").addClass("invisible-footer");
			jQuery("html").css("background-color", "#0F3D53");
		}
		document.body.style.overflow = 'hidden';
		jQuery("body").addClass("new-background");
		//jQuery("#contact-button a.inner").animate({opacity: '0.5'}, 500);
		jQuery(".overlay").fadeIn('slow', function() {
			jQuery('#contact-modal-window').show().animate({height: '522px'}, 800, function() {
				jQuery(".contact-background ").animate({opacity: '1.0'}, 500, function() {
					jQuery("#title-contact").animate({right: '18px', opacity: '1.0'}, 700, function() {
						jQuery("#content-contact").animate({right: '18px', opacity: '1.0'}, 650, function() {
							jQuery("#google-map").animate({right: '18px', opacity: '1.0'}, 650);

						});

					});

				});
			});
		});
	});


	jQuery("a.close-button").click(function() {

		jQuery("#google-map").animate({right: '50px', opacity: '0'}, 350);
		jQuery("#content-contact").animate({right: '50px', opacity: '0', width: '419'}, 500);

		jQuery("#title-contact").animate({right: '50px', opacity: '0'}, 500, function() {
			jQuery(".contact-background ").animate({opacity: '0'}, 100, function() {
				jQuery('#contact-modal-window').animate({height: '0px'}, 400, function() {
					jQuery(this).hide();
					jQuery(".overlay").fadeOut("slow", function() {
						//jQuery("#contact-button a.inner").css("background-position","0 0px");
						jQuery("#contact-button a.inner").animate({opacity: '1.0'}, 500);
						if (jQuery('.full-width') != null) {
							jQuery(".full-width").animate({opacity: '1.0'}, 500);
						}
						if (jQuery('#content') != null) {
							jQuery("#content").css("visibility", "visible");
							jQuery("#content-footer").removeClass("invisible-footer");
							jQuery("html").css("background-color", "#255C77");
						}
						document.body.style.overflow = 'auto';
						//jQuery("#content").animate({opacity: '1.0'}, 500);
						jQuery("body").removeClass("new-background");
					});
				});

			});
		});

	});

});