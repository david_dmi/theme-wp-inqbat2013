//To include functionality on logos clients
//window.onload=logo_slider;
jQuery("#logos-clients").ready(function() {
	logo_slider();
});
function logo_slider() {
	jQuery("#logos-clients").show();

	maxw = 0;
	jQuery('.slides li img').each(function() {
		w = jQuery(this).width();
		if (w > maxw)
			maxw = w;
	});


	if (jQuery("#logos-clients").length > 0) {
		jQuery('#logos-clients img').css('visibility', 'visible').fadeIn();
		jQuery('#logos-clients').flexslider({
			animation: "slide",
			animationLoop: true,
			reverse: false,
			/*itemWidth: 232,*/
			itemWidth: maxw,
			itemMargin: 6,
			minItems: 1,
			maxItems: 6,
			controlNav: true,
			directionNav: false,
			slideshowSpeed: 32000, //Integer: Set the speed of the slideshow cycling, in milliseconds
			animationSpeed: 800
		});
	}

	if (jQuery(".flex-control-nav").length > 0) {
		//h = jQuery(".slides li").height();
		//console.log(h);
		jQuery(".flex-control-nav").css('bottom', '-35px');
	}

	jQuery('.slides li img').each(function() {
		w = jQuery(this).width();
		jQuery("#logos-clients .slides li").attr('style', "width: " + w + "px; min-width:" + w + "px; float: left; display: block;");
	});

}

// Facebook Like
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id))
		return;
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


// Suport Mobile and table menu wrap
jQuery(function() {
	if (jQuery.browser.msie && jQuery.browser.version.substr(0, 1) < 7)
	{
		jQuery('li').has('ul').mouseover(function() {
			jQuery(this).children('ul').css('visibility', 'visible');
		}).mouseout(function() {
			jQuery(this).children('ul').css('visibility', 'hidden');
		})
	}
	jQuery('#menu-wrap').prepend('<div id="menu-trigger">Menu</div>');
	jQuery("#menu-trigger").on("click", function() {
		jQuery(".nav-menu").slideToggle();
	});
});

/* iPad */
var isiPad = navigator.userAgent.match(/iPad/i) != null;
if (isiPad)
	jQuery('#menu ul').addClass('no-transition');   