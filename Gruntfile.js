module.exports = function(grunt) {

	// Initialise Grunt
	grunt.initConfig({

		// Core LESS compiling
		less: {
			development: {
				options: {
					paths: ["./less"],
					cleancss: false,
					compress: true,
					yuicompress: false,
					optimization: 2,
					strictImports: true,
					sourceMap: true,
					sourceMapFilename: './theme-bootstrap.css.map',
					sourceMapBasepath: './',
				},
				files: {
					"./theme-bootstrap.css": "./less/style.less"
				}
			},
			production: {
				options: {
					paths: ["./less"],
					cleancss: true,
					compress: true,
					strictImports: true,
				},
				files: {
					"./theme-bootstrap.css": "./less/style.less"
				}
			}
		},

		// ensures that any basic javascript syntax errors are picked up before compiling
		jshint: {
			// define the files to lint
			files: ['Gruntfile.js', './js/*.js'],
			// configure JSHint (documented at http://www.jshint.com/docs/)
			options: {
				// more options here if you want to override JSHint defaults
				globals: {
					jQuery: true,
					console: true,
					module: true
				}
			}
		},

		// Our main watch task - keeping an eye on files and folders and compiling them on any changes
		watch: {
			files: ["./less/*", "./bower_components/bootstrap/less/*"],
			tasks: ["jshint","less"]
		}
	});

	// Load the various grunt libraries needed
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// grunt.registerTask('default', ['jshint', 'less']);
	grunt.registerTask('default', ['less:development']);
};