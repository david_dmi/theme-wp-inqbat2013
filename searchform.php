<?php
/**
 * Searchform
 *
 * Custom template for search form
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */
?>

<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="search-form">
		<input type="text" name="s" id="s" placeholder="<?php esc_attr_e( 'Search...', 'qproject' ); ?>" />
		<input type="submit" class="postfix button expand" name="submit" id="searchsubmit" value="<?php //esc_attr_e( 'Buscar...', 'qproject' ); ?>" />
	</div>
</form>
