<?php
/**
 * Header
 *
 * Setup the header for our theme
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>

    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

    <!-- Set the viewport width to device width for mobile -->
<!--    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php wp_title(); ?></title>

    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/><![endif]-->

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php bloginfo('template_directory') ?>/favicon.ico" type="image/x-icon"/>

    <!-- Apple Touch Icon -->
    <link rel="apple-touch-icon" href="<?php bloginfo('template_directory') ?>/images/icon-sm.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory') ?>/images/icon-med.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory') ?>/images/icon-lg.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory') ?>/images/icon-bg.png"/>

    <link rel="apple-touch-startup-image" href="<?php bloginfo('template_directory') ?>/images/startup.png">

    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>

	<?php wp_head(); ?>

    <!--Selectivizr-->
    <!--[if (gte IE 6)&(lte IE 8)]>
    <script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/selectivizr.js"></script>
    <noscript>
        <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/style.css"/>
    </noscript>
    <![endif]-->

    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script>(function () {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
			}
			_fbq.push(['addPixelId', '1475950325979862']);
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none"
                   src="https://www.facebook.com/tr?id=1475950325979862&amp;ev=NoScript"/></noscript>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body <?php body_class(); ?>>
<!-- Facebook Like -->
<div id="fb-root"></div>
<!--	<script type="text/javascript">(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
				return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>-->

<!-- Suport Mobile and table menu wrap -->
<script type="text/javascript">
	jQuery(function () {
		if (jQuery.browser.msie && jQuery.browser.version.substr(0, 1) < 7) {
			jQuery('li').has('ul').mouseover(function () {
				jQuery(this).children('ul').css('visibility', 'visible');
			}).mouseout(function () {
				jQuery(this).children('ul').css('visibility', 'hidden');
			})
		}
//		jQuery('#menu-wrap').prepend('<div id="menu-trigger">Menu</div>');
//		jQuery("#menu-trigger").on("click", function(){
//		    jQuery(".nav-menu").slideToggle();
//		});
	});

	//Handle submenu for small (< 768px) devices
	jQuery(document).ready(function () {

		var sub_status = false;

		jQuery('.submenu').click(function () {
			console.log("clicked");
			if (sub_status) {
				jQuery('.submenu-links').hide().css('height', '0');
				jQuery('.submenu h1 i').removeClass('fa-minus').addClass('fa-plus');
				sub_status = false;
			}
			else {
				jQuery('.submenu-links').show().css('height', 'auto');
				jQuery('.submenu h1 i').removeClass('fa-plus').addClass('fa-minus');
				sub_status = true;
			}
		});
	});
</script>

<!--Contact us animation script -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/animation.js"></script>
<!--[if lte IE 8]>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/animation-ie8.js"></script>
<html class="ie8">
<![endif]-->

<header id="header">
    <div id="pre-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-xs-12">
                    <a href="https://www.agileana.com" target="_blank" title="Go to Agileana, an inQbation partner"><img src="<?php bloginfo('template_directory'); ?>/images/agileana-logo-vertical-sm.png" alt="Agileana, inQbation partner"></a>
                    <span class="to-left"> | </span>
                    <a href="https://www.balystic.com" target="_blank" title="Go to Balystic, an inQbation partner"><img src="<?php bloginfo('template_directory'); ?>/images/balystic-logo-sm.png" alt="Balystic, inQbation partner"></a>
                </div>
                <div class="col-sm-7 text-right">
                    <span class="pre-header-tel hidden-xs">Telephone: <a href="tel:703.999.1232">703.999.1232</a></span>
                    <span class="hidden-xs"> | </span>
                    <span class="pre-header-email">Email: <a href="mailto:results@agileana.com">results@inqbation.com</a></span>
                </div>
            </div>
        </div>
    </div>

<!--    <div class="container">-->
    
    <div class="container-fluid">
    <!-- <div class="container"> -->
        <div class="row">
            <div class="site-title col-md-4 col-sm-7">
                <?php if (is_front_page()): ?>
                    <h1 id="branding">
                        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php //bloginfo( 'name' );  ?>
                            <img id="logo-header" src="<?php echo get_bloginfo('template_directory') . '/images/inqbation-logo.svg'; ?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"/>
                        </a>
                    </h1>
                    <h2 class="subheader"><?php bloginfo('description'); ?></h2>
                <?php else: ?>
                    <span id="branding">
                            <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" el="home"><?php //bloginfo( 'name' );  ?>
                                <img id="logo-header" src="<?php echo get_bloginfo('template_directory') . '/images/inqbation-logo.svg'; ?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"/>
                            </a>
                        </span>
                    <span class="subheader"><?php bloginfo('description'); ?></span>
                <?php endif; ?>
            </div>

            <div class="col-md-8 col-sm-5">
                <div id="menu-wrap" class="header-container-right custom-dropdown">
                    <?php wp_nav_menu(array('theme_location' => 'header-menu', 'menu_id' => 'menu', 'menu_class' => 'nav-menu', 'fallback_cb' => 'qproject_page_menu', 'container' => 'nav', 'container_class' => 'header_menu', 'walker' => new qproject_navigation())); ?>
                </div>
                <!-- <div class="language-english col-md-1"><a href="http://www.inqbation.com/" title="English version" class="english" target="_blank">English version</a></div>-->
            </div>
        </div>
    </div>
</header>

<div id="background_texture">
    <div class="container">
        <div class="row">
            <div id="share-icons-internal" class="col-md-12">
                <ul>
                    <li>Share:</li>

                    <li><a title="inQbation on Twitter" class="twitter-icon twitter popup" href="http://twitter.com/share?text=Northern%20Virginia%20web%20designer%20developer%20company%20for%20WordPress%20Drupal%20CMS&amp;url=<?php bloginfo('url'); ?>&amp;via=inQbation&amp;lang=en" target="_blank">Twitter</a></li>
                    <li>
                        <a href="http://www.facebook.com/sharer.php?u=<?php bloginfo('url'); ?>&amp;t=Northern%20Virginia%20web%20designer%20developer%20company%20for%20WordPress%20Drupal%20CMS" target="_blank" title="inQbation on Facebook" class="facebook-icon">Facebook</a></li>
                    <li>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php bloginfo('url'); ?>&amp;title=Northern%20Virginia%20web%20designer%20developer%20company%20for%20WordPress%20Drupal%20CMS&amp;summary=&amp;source=" target="_blank" title="inQbation on LinkedIn" class="linkedin-icon">LinkedIn</a></li>
                    <li>
                        <a href="http://pinterest.com/pin/create/button/?url=<?php bloginfo('url'); ?>&amp;description=Northern%20Virginia%20web%20designer%20developer%20company%20for%20WordPress%20Drupal%20CMS" target="_blank" title="inQbation on Pinterest" class="pinterest-icon">Pinterest</a></li>
                    <li><a href="https://plus.google.com/share?url=<?php bloginfo('url'); ?>&amp;hl=en" target="_blank" title="inQbation on Google+" class="google-plus-icon">Google+</a></li>

            </div>
        </div>
    </div>
</div>

