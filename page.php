<?php
/**
 * Page
 *
 * Loop container for page content
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */

get_header(); ?>

	<div id="wrapper" class="container">
	<div class="row">

    <?php if ( have_posts() ) : ?>

        <?php while ( have_posts() ) : the_post(); ?>

        <!-- Main Content -->
        <div id="content" class="col-md-9 col-md-push-3" role="content">

            <div class="hidden-md hidden-lg submenu">
                <h1><?php the_title(); ?> <i class="fa fa-plus pull-right"></i></h1>
                <?php include_once(TEMPLATEPATH .'/submenu.php'); ?>
            </div><!-- block-sm submenu -->

            <div class="content-inside">

                <div id="breadcrumbs" class="col-md-12">
                    <?php
                        if(function_exists('bcn_display')){
                            bcn_display();
                        }
                    ?>
                </div>

                <?php get_template_part( 'content', 'page' )?>

                <?php endwhile; ?>

                <?php /*
                <div class="block-sm text-center">
                    <!-- <a href="http://www.inqbation.com/gsa-541-aims-contract-holder/" title="Government Buyer? GSA">
                        <img src="<?php echo get_bloginfo('template_directory').'/images/government-buyer-gsa-logo.png'; ?>"  alt="Government Buyer? GSA" />
                    </a> -->
                    <a href="<?php echo get_page_link(9166); ?>" title="Government Buyer? GSA">
                        <img src="<?php echo get_bloginfo('template_directory').'/images/government-buyer-gsa-logo.png'; ?>"  alt="Government Buyer? GSA" />
                    </a>
                    <img src="<?php echo get_bloginfo('template_directory').'/images/mbe-logo.png'; ?>"  alt="Minority Business Enterprise" title="Minority Business Enterprise" />
                </div>
 */ ?>

            </div><!-- End of content-inside -->
        </div><!-- End of content --> 

        <aside class="custom-left-menu-sidebar col-md-3 col-md-pull-9">
                <?php include_once(TEMPLATEPATH .'/sidebar-left-menu.php'); ?>
        </aside>

    <?php endif; ?>



<?php get_footer(); ?>