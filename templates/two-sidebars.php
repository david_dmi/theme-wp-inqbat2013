<?php

/*
 * Template Name: Two Sidebars Template
 */
 
get_header(); ?>

    <?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>
    <aside class="custom-left-menu-sidebar alpha grid_5 push_18">
        <div id="sidebar-left">
            <?php include_once(TEMPLATEPATH .'/sidebar-left-menu.php'); ?>
        </div>1

        
    </aside>

    <!-- Main Content -->
    <div id="content" class="grid_18 prefix_1 omega pull-5" role="content">

        <div class="content-inside">

        <!-- Breadcrumbs -->
        <div id="breadcrumbs" class="grid_13 suffix_5 alpha omega">
            <?php
                if(function_exists('bcn_display')){
                    bcn_display();
                }
            ?>
        </div>

    	<!-- Right Sidebar -->
    	<?php get_sidebar(); ?>

        <?php get_template_part( 'content', 'page' )?>
        <?php endwhile; ?>

        </div><!-- End of content-inside -->
    </div><!-- End of content --> 

    <?php endif; ?>

<?php get_footer(); ?>