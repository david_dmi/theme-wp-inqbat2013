<?php

/*
 * Template Name: Support Template
 */

get_header(); ?>

    <!-- Main Content -->
    <div id="content" class="grid_24" role="content">

        <div class="content-inside">

    	<!-- Breadcrumbs -->
    	<div id="breadcrumbs" class="grid_19 suffix_5 alpha omega">
    	    <?php
    	        if(function_exists('bcn_display')){
    	            bcn_display();
    	        }
    	    ?>
    	</div>

    	
		<div class="grid_16 alpha omega">
		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; ?>
			
		<?php endif; ?>
		</div>
        
        <!-- Sidebar -->
		<aside id="sidebar-right" class="sidebar-page grid_8 omega">

		<?php if ( dynamic_sidebar('Sidebar Support') ) : elseif( current_user_can( 'edit_theme_options' ) ) : ?>

			<h5><?php _e( 'No widgets found.', 'foundaton' ); ?></h5>
			<p><?php printf( __( 'It seems you don\'t have any widgets in your sidebar! Would you like to %s now?', 'qproject' ), '<a href=" '. get_admin_url( '', 'widgets.php' ) .' ">populate your sidebar</a>' ); ?></p>

		<?php endif; ?>

		</aside>
		<!-- End Sidebar -->
        </div><!-- End of content-inside -->
    </div><!-- End of content --> 

<?php get_footer(); ?>