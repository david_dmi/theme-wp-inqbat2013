<?php

/*
 * Template Name: Sidebar Right Template
 */

get_header(); ?>

    <!-- Main Content -->
    <div id="content" class="grid_24" role="content">

        <div class="content-inside">

    	<!-- Breadcrumbs -->
    	<div id="breadcrumbs" class="grid_19 suffix_5 alpha omega">
    	    <?php
    	        if(function_exists('bcn_display')){
    	            bcn_display();
    	        }
    	    ?>
    	</div>

    	<!-- Right Sidebar -->
    	<?php get_sidebar(); ?>

		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; ?>
			
		<?php endif; ?>

        </div><!-- End of content-inside -->
    </div><!-- End of content --> 

<?php get_footer(); ?>