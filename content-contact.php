<?php
/**
 * Content Page
 *
 * Loop content in page template (page.php)
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */
?>

<article>
	<!-- 
	<header>
		<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'qproject' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	</header> -->

	<header>
		<h1><?php the_title(); ?></h1>
	</header>

	<!-- 	<?php if ( has_post_thumbnail()) : ?>
		<a href="<?php the_permalink(); ?>" class="th" title="<?php the_title_attribute(); ?>" ><?php the_post_thumbnail(); ?></a>
		<?php endif; ?> -->

<div id="background-map">
	<div class="contact-content grid_10 sufix_1">
		<div class="contact-information">
			<?php the_content(); ?>
		</div>
	</div>
</div>

</article>