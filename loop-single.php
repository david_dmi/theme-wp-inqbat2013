<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <h1 class="entry-title"><?php the_title(); ?></h1>

					<small class="m-t-10" id="link-block">
						<i class="fa fa-user"></i> <?php the_author_posts_link(); ?>  &nbsp;&nbsp;&nbsp;
						<i class="fa fa-calendar"></i> <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date('F, Y'); ?></time> &nbsp;&nbsp;&nbsp;
						<i class="fa fa-tags"></i>  <?php the_category(', '); ?>
					</small>
			        <?php if (has_post_thumbnail()): ?>
			          <div class="single-feature-image mb-5">
			            <?php the_post_thumbnail('single-page-big', array( 'class' => 'img-responsive aligncenter' )); ?>
			          </div>
			        <?php endif; ?>
					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'qproject' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->


                    <hr><div id="share-buttons" class="">
                        <ol class="list-inline">
                            <li><span class="title-follow">Share:</span></li>
                            <?php
                                $my_url = urlencode(get_permalink($post->ID));
                                $my_title = urlencode(get_the_title($post->ID));
                                $my_text = urlencode( str_replace( "<a class=\"read-more\" href=\"".get_permalink($post->ID)."\">Read more <span class=\"meta-nav\">&raquo;</span></a>", "", get_the_excerpt($post->ID) ));

                                if ( has_post_thumbnail($post->ID)){
                                    $my_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID));
                                    $my_img = $my_img[0];
                                } else {
                                    $my_img = get_bloginfo('template_directory');
                                    $my_img = $my_img . "/images/inQbation-thumbnail.jpg";
                                }
                                $my_img = urlencode($my_img);
                            ?>

                            <li><a href="https://twitter.com/share?text=<?php echo $my_title; ?>&amp;url=<?php echo $my_url; ?>&amp;via=inQbation&amp;lang=en" title="Share on Twitter" target="_blank" class="twitter-icon twitter popup icon">Twitter</a></li>

                            <li><a href="http://www.facebook.com/sharer.php?s= 100&amp;p[title]=<?php echo $my_title; ?>&amp;p[url]=<?php echo $my_url; ?>&amp;p[images][0]=<?php echo $my_img; ?>&amp;p[summary]=<?php echo $my_text; ?>" target="_blank" title="Share on Facebook" class="facebook-icon icon">Facebook</a></li>

                            <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $my_url; ?>&amp;title=<?php echo $my_title; ?>&amp;summary=<?php echo $my_text; ?>" target="_blank" title="Share on LinkedIn" class="linkedin-icon icon">LinkedIn</a></li>

                            <li><a href="http://pinterest.com/pin/create/button/?url=<?php echo $my_url; ?>&amp;media=<?php echo $my_img; ?>&amp;description=<?php echo $my_title; ?>" target="_blank" title="Share on Pinterest" class="pinterest-icon icon">Pinterest</a></li>
                            
                            <li><a href="https://plus.google.com/share?url=<?php echo $my_url; ?>&amp;hl=en" target="_blank" title="Share on Google+" class="google-plus-icon icon">Google+</a></li>
                            
                        </ol>
                    </div> <!-- .follow-buttons-post -->

<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
	<?php get_template_part('author-box'); ?>
<?php endif; ?>

					<div class="entry-utility">
						<?php inqbation_posted_in(); ?>
						<?php edit_post_link( __( 'Edit', 'qproject' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-utility -->
				</div><!-- #post-## -->

				<?php comments_template( '', true ); ?>
				
				<!--div id="nav-below" class="navigation">
					<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'qproject' ) . '</span> %title' ); ?></div>
					<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'qproject' ) . '</span>' ); ?></div>
				</div>< #nav-below -->

				

<?php endwhile; // end of the loop. ?>