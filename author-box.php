<?php
/**
 * Author Box
 *
 * Displays author box with author description and thumbnail on single posts
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */
?>

<section class="panel">
	<div class="panel-body">
		<div class="col-sm-2"><a class="img-responsive" href="<?php get_the_author_meta('url'); ?>"><?php echo get_avatar( get_the_author_meta('user_email'),'100' ); ?></a></div>
		<div class="col-sm-10">
			<h2><?php _e('About', 'qproject' ); ?> <?php the_author_link(); ?></h2>
			<div>
				<?php echo get_the_author_meta('description'); ?>
			</div>
			<div id="author-link">
				<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
					<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'qproject' ), get_the_author() ); ?>
				</a>
			</div><!-- #author-link	-->
		</div>
	</div>
</section>