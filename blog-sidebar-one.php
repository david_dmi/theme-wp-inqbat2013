<div id="sidebar-blog-left" class="sidebar-blog col-md-2 col-md-pull-8">
	<div class="inner">
		<div class="search-posts">
			<?php if (is_active_sidebar('qproject_sidebar_blog')) : ?>
				<?php dynamic_sidebar('qproject_sidebar_blog'); ?>
			<?php endif; ?>
		</div>

	</div>
</div>

<!--<script type="text/javascript">-->
<!--	jQuery(document).ready(function () {-->
<!--		jQuery(".widget_sidebartabs .prev").hide();-->
<!--		jQuery(".widget_categories #cat").attr("title", "Select a category");-->
<!--	});-->
<!--</script>-->