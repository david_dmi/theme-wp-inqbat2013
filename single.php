<?php get_header(); ?>

<div id="wrapper" class="container-fluid">
	<div class="row">

		<div id="content" class="single">

			<div id="blog-content" class="content-blog content-inside col-md-8 col-md-push-2">

				<div id="breadcrumbs">
					<?php
					if (function_exists('bcn_display')):
						bcn_display();
					endif;
					?>
				</div>

				<div id="main-posts">
					<?php get_template_part('loop', 'single'); ?>
				</div>

			</div><!-- End of content-inside -->

			<?php //setPostViews(get_the_ID()); ?>

			<?php get_template_part('blog-sidebar-one'); ?>

			<?php get_template_part('blog-sidebar-two'); ?>

		</div><!-- End of content -->

		<?php get_footer(); ?>
