<?php
/*
 * 404 Error page
 *
 * @package WordPress
 * @subpackage qproject, for WordPress
 * @since qproject, for WordPress 1.0
 */

get_header(); ?>

<?php /* If there are no posts to display, such as an empty archive page */ ?>
<div id="content" class="grid_24" role="content">

    <!-- <div class="ribbons"></div> -->

    <div class="content-inside">
        
        <div class="post error404 not-found search-results grid_24 alpha omega">
            <div class="post-inside">
                <div class="logo">
                    <hgroup class="site-title">
                        <h1 id="branding">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php //bloginfo( 'name' ); ?>
                                <img id="logo-header" src="<?php echo get_bloginfo('template_directory').'/images/inqbation-logo.png'; ?>"  alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
                            </a>
                        </h1>
                        <h2 class="subheader"><?php bloginfo('description'); ?></h2>
                    </hgroup>
                </div>
                <hgroup>
                    <h2 class="error"><?php _e( '404' ); ?></h2>
                    <h3>we are sorry!</h3>
                </hgroup>

                <div class="entry-content">
                    <p>This page may not exist or is not available at the time. <br> Try a new search.</p>
                    <?php get_search_form(); ?>
                </div><!-- .entry-content -->

                <div class="classic-menu">
                    <ul>
                        <li><a href="<?php bloginfo('home')?>" title="<?php echo bloginfo('name')?>">Home</a></li>
                        <li><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'contact' ) ) ); ?>" title="">Contact Us</a></li>
                    </ul>
                </div>            
                <div class="project-manager"></div>
            </div>
        </div><!-- #post-0 -->
    </div><!-- End of content-inside -->
</div><!-- End of content --> 


<!--<?php //get_footer(); ?>-->